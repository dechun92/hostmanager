//! This module contains the `Label` type used to identify managed host entries.

use std::fmt;
use std::str::FromStr;
use std::error::Error;


/// A wrapper for `String` comments with a parse method that disallows newlines.
#[derive(Clone, Debug, PartialEq)]
pub struct Comment {
    comment: String,
}

impl Comment {
    /// Public API for deciding if the comment is empty. Used to determine whne
    /// to print the comment in output.
    pub fn is_not_empty(&self) -> bool {
        self.comment.len() > 0
    }
}

impl fmt::Display for Comment {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.comment)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CommentParseError;

impl Error for CommentParseError {
    fn description(&self) -> &str {
        "invalid comment: line breaks are not allowed"
    }
}


impl fmt::Display for CommentParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.description())
    }
}

impl FromStr for Comment {
    type Err = CommentParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {

        let maybe_line = s.find('\n').or(s.find('\r'));

        if maybe_line.is_some() {
            Err(CommentParseError)
        } else {
            let comment = Comment { comment: s.to_string() };
            Ok(comment)
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_valid_comment() {
        let comment = "one two three";
        let res: Result<Comment, _> = comment.parse();
        assert!(res.is_ok());
        assert_eq!(res.unwrap().comment, comment);
    }

    #[test]
    fn test_invalid_comment() {
        let comment = "one two\nthree";
        let res: Result<Comment, _> = comment.parse();
        assert!(res.is_err());
        assert_eq!(res.unwrap_err(), CommentParseError);
    }
}
