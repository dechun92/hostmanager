//! Struct and associated methods for representing an entire `/etc/hosts` file.

use std::collections::BTreeMap;
use std::net::IpAddr;
use console::style;

use comment::Comment;
use label::Label;
use hostnames::Hostnames;
use parser::host_file_parser;
use printer::Printable;
use host_entry::HostEntry;


/// This is an intermediate structure that `parser.rs` can use to capture both
/// managed entries from the hosts file (that is, entries this program created
/// and and can modify) and unmanaged entries (any other commands or manually
/// entered host entries).
#[derive(PartialEq, Clone, Debug)]
pub enum HostLines {
    Unmanaged(String),
    Managed((Label, HostEntry)),
}


/// The `HostsFile` type uses vectors to hold all of the managed and unmanaged
/// parts of a given host file.
#[derive(Debug)]
pub struct HostsFile {

    managed_entries: BTreeMap<Label, HostEntry>,
    unmanaged_entries: Vec<String>,

    // an optional field that will tell this struct whether to limit output
    // by active entries (`Some(true)`), inactive entries (`Some(false)`),
    // or all entries (`None`)
    active_filter: Option<bool>,
}


impl HostsFile {
    /// A `HostsFile` can be constructed from a vector of `HostLines`.
    pub fn from_host_lines(mut entries: Vec<HostLines>) -> HostsFile {
        let mut hf = HostsFile { managed_entries: BTreeMap::new(),
                                 unmanaged_entries: Vec::new(),
                                 active_filter: None,
        };

        for entry in entries.drain(..) {
            match entry {
                HostLines::Unmanaged(u)    => hf.unmanaged_entries.push(u),
                HostLines::Managed((l, e)) => { hf.managed_entries.insert(l, e); },
            }
        }

        hf
    }

    /// If we have the contents of the actual `/etc/hosts` file, we can first
    /// parse it into `HostLines` and then convert it to our `HostsFile` struct.
    pub fn from_hosts_file(mut contents: String) -> HostsFile {
        // this is a bit of a hack: if someone removes a trailing newline from
        // one of our managed host entries, we add it back here because our
        // parser (rightly) requires newlines after managed entries.
        if !contents.ends_with("\n") { contents.push_str("\n"); }

        let contents = contents.into_bytes();
        let result = host_file_parser(&contents).to_result();
        match result {
            Ok(entries) => HostsFile::from_host_lines(entries),
            Err(e)      => {
                println!("Parse error: {:?}", e);
                panic!("Unrecoverable error: could not parse hosts file");
            }
        }
    }

    /// Public API for adding a new host entry. Since our program's `--add`
    /// mode requires all of the arguments, we can construct the new instance
    /// directly using `HostEntry::new`.
    ///
    /// This will fail if the requested `label` or any requested `hosts` are
    /// already present.
    pub fn add_new_entry(&mut self,
                         label: Label,
                         ip_address: IpAddr,
                         hosts: Hostnames,
                         comment: Comment) -> Result<(), String> {

        // exit early if we find the key `label` is already present
        if self.managed_entries.get(&label).is_some() {
            return Err(format!("Requested label '{}' already in use", label));
        }

        match self.has_duplicate_hosts(&label, &hosts) {
            Err(err) => Err(err),
            Ok(_)  => {
                let entry = HostEntry::new(ip_address, hosts, comment, true);
                self.managed_entries.insert(label, entry);
                Ok(())
            }
        }
    }

    /// Public API for modifying a host entry. The `label` is required in order
    /// to identify the entry, but all other arguments are optional.
    ///
    /// This will fail if any requested `hosts` are already present (in an entry
    /// other than this one, or if the supplied `label` isn't found.
    pub fn modify_entry(&mut self,
                        label: Label,
                        ip_address: Option<IpAddr>,
                        hosts: Option<Hostnames>,
                        comment: Option<Comment>,
                        active: Option<bool>) -> Result<(), String> {

        if let Some(ref hs) = hosts {
            self.has_duplicate_hosts(&label, &hs)?;
        }
        self.managed_entries
            .get_mut(&label)
            .map(|e| e.modify(ip_address, hosts, comment, active))
            .ok_or(format!("No entry found with label '{}'", label))
    }

    /// API for removing the entry identified by `label`. Will return an error
    /// if `label` is not found.
    pub fn remove_entry(&mut self, label: &Label) -> Result<(), String> {
        let maybe_removed = self.managed_entries.remove(&label);

        match maybe_removed {
            Some(_v) => Ok(()),
            None => Err(format!("No entry found with label '{}'", label))
        }
    }


    /// Allows you to rename a given host entry. Will fail if `old_name` is not
    /// found.
    pub fn rename_entry(&mut self,
                        old_name: Label,
                        new_name: Label) -> Result<(), String> {

        // exit early if the requested `new_name` is already taken.
        if self.managed_entries.contains_key(&new_name) {
            return Err(format!("Requested name {} already in use",
                               new_name)
            );
        }

        let matched_entry = self.managed_entries.remove(&old_name);

        match matched_entry {
            None      => Err(format!("No entry named: {}", old_name)),
            Some(entry) => {
                self.managed_entries.insert(new_name, entry);
                Ok(())
            }
        }
    }

    /// This will set `active_filter` on the current struct to optionally filter
    /// console output by entries that are active or inactive.
    pub fn set_filter(&mut self, active_filter: Option<bool>) {
        self.active_filter = active_filter;
    }


    /// The `status` command allows callers to view the status of all managed
    /// entries, or a specific entry if `Some(label)` is supplied.
    /// Will return an error if a `label` is given but not found.
    pub fn status(&self, label: Option<&Label>) -> Result<String, String> {
        match label {
            Some(n) => self.single_entry_status(&n)
                .ok_or(format!("No entry named '{}'", n)),
            None    => Ok(self.format_console()),
        }
    }

    /// Given a `label`, will return the status of that `label` (if present)
    /// or `None`.
    fn single_entry_status(&self, label: &Label) -> Option<String> {
        match self.managed_entries.get(&label) {
            None        => None,
            Some(ref e) => {
                let status = e.is_active();
                let mut s = format!("{} ({})\n",
                                    label.format_by_status(status),
                                    e.active_string());
                s.push_str(&e.format_console());
                Some(s)
            }
        }
    }

    /// Private helper to check if the requested `new_hosts` are already in
    /// another entry. This checks for hosts with the same label, and any
    /// managed or unmanaged hosts sharing hostnames with `new_hosts`.
    fn has_duplicate_hosts(&self,
                           new_label: &Label,
                           new_hosts: &Hostnames) -> Result<(), String> {

        let maybe_dup_managed = self.managed_entries
            .iter()
            .find(|&(key, entry)| entry.has_any_hosts(new_hosts) && key == new_label)
            .map(|ref h| format!("ERROR: Requested hosts {} already in entry {:?}",
                                 new_hosts,
                                 h));

        let maybe_dup_unmanaged = self.check_for_unmanaged_duplicates(new_hosts);
        match maybe_dup_managed.or(maybe_dup_unmanaged) {
            Some(matched) => Err(matched),
            None          => Ok(()),
        }
    }


    /// This private function will parse any unmanaged line from an `/etc/hosts`
    /// file. If the line is a valid host entry, it will check that `new_hosts`
    /// don't duplicate any of the hosts in the entry.
    fn check_for_unmanaged_duplicates(&self,
                                      new_hosts: &Hostnames) -> Option<String> {

        let mut res = None;

        for line in &self.unmanaged_entries {

            let has_duplicate = Hostnames::from_raw_host_entry(&line)
                .map(|hs| hs.contains_any(new_hosts))
                .unwrap_or(false);

            if has_duplicate {
                res = Some(line.clone());
                break;
            }
        }
        res

    }

    /// This function is used to write the contents of `HostsFile` to
    /// `/etc/hosts`.
    pub fn format_all(&self) -> String {
        let mut s = String::new();

        for entry in &self.unmanaged_entries {
            s.push_str(&entry);
        }

        for (label, entry) in self.managed_entries.iter() {
            let line = format!("### <hostmanager {} {}", &label.format(), &entry.format());
            s.push_str(&line)
        }

        s
    }

}


impl Printable for HostsFile {

    /// Allows us to nicely format all of the managed entries for terminal
    /// output.
    fn format_console(&self) -> String {
        let mut s = String::new();

        // checks whether or not we should display the entry
        let display_entry = |e: &HostEntry| match self.active_filter {
            Some(a) => e.is_active() == a,
            None    => true,
        };

        for (label, entry) in self.managed_entries.iter() {
            if display_entry(entry) {

                let formatted_label = if entry.is_active() {
                    format!("{} ({})\n", &style(&label).bold().green(), "active")
                } else {
                    format!("{} ({})\n", &style(&label).bold().red(), "inactive")
                };

                s.push_str(&formatted_label);
                s.push_str(&entry.format_console());
            }
        }
        s
    }

}



#[cfg(test)]
mod test {
    use super::*;
    use std::net::{IpAddr};

    fn get_sample_file() -> String {
        let sample = r####"192.168.1.10 duplicate
127.0.0.1 localhost

##
# IP V6 localhost
# #
::1 localhost

### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

### <hostmanager label="example2" comment=hi there>
# 192.168.1.11 notused

"####;
        String::from(sample)
    }

    #[test]
    fn duplicate_hosts_present_inline_comment_test() {
        let sample = r####"### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

192.168.1.10 inlineduplicate # here is an inline comment

"####;
        let host_strings: Hostnames = "siteone sitetwo inlineduplicate".parse().unwrap();
        let hosts = HostsFile::from_hosts_file(sample.to_string());
        let label = "test".parse().unwrap();
        let res = hosts.has_duplicate_hosts(&label, &host_strings);
        assert!(res.is_err());
    }

    #[test]
    fn duplicates_hosts_present_test() {
        let host_strings: Hostnames = "siteone sitetwo duplicate".parse().unwrap();
        let hosts = HostsFile::from_hosts_file(String::from(get_sample_file()));
        let label = "test".parse().unwrap();
        let res = hosts.has_duplicate_hosts(&label, &host_strings);
        assert!(res.is_err());
    }

    #[test]
    fn duplicates_absent_test() {
        let host_strings: Hostnames = "siteone sitetwo allgood".parse().unwrap();
        let sample_file = get_sample_file();
        let hosts = HostsFile::from_hosts_file(String::from(sample_file));
        let expected = Ok(());
        let label = "test".parse().unwrap();
        let res = hosts.has_duplicate_hosts(&label, &host_strings);
        assert_eq!(res, expected);
    }

    #[test]
    fn to_and_from_hosts_file_test() {
        let expected = get_sample_file();
        let hosts = HostsFile::from_hosts_file(expected.clone());
        let res = hosts.format_all();
        assert_eq!(expected, res);
    }


    #[test]
    fn reformat_hosts_file_test() {
        let original = r####"### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

192.168.1.10 localsite

"####;

        let expected = r####"192.168.1.10 localsite

### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

"####;
        let hosts = HostsFile::from_hosts_file(String::from(original));
        let res = hosts.format_all();

        assert_eq!(expected, res);
    }

    #[test]
    // regression test for host files with no managed entries
    fn test_no_managed_entries_unchanged() {
        let original = r####"# #
# Host Database
# 
# localhost is used to configure the loopback interface
# when the system is booting.  Do not change this entry.
# #
127.0.0.1 localhost

255.255.255.255 broadcasthost

::1 localhost


"####;
        let hosts = HostsFile::from_hosts_file(String::from(original));
        let res = hosts.format_all();

        assert_eq!(original, res);
    }

    #[test]
    fn activate_entry() {
        let original = r####"192.168.1.10 localsite

### <hostmanager label="example" comment=example>
# 127.0.0.1 example.com foo bar

"####;

        let expected = r####"192.168.1.10 localsite

### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

"####;
        let mut hosts = HostsFile::from_hosts_file(String::from(original));
        let label = "example".parse().unwrap();
        let res = hosts.modify_entry(label, None, None, None, Some(true));
        assert!(res.is_ok());
        assert_eq!(hosts.format_all(), expected);
    }

    #[test]
    fn deactivate_entry() {
        let original = r####"192.168.1.10 localsite

### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

"####;

        let expected = r####"192.168.1.10 localsite

### <hostmanager label="example" comment=example>
# 127.0.0.1 example.com foo bar

"####;
        let mut hosts = HostsFile::from_hosts_file(String::from(original));
        let label = "example".parse().unwrap();
        let res = hosts.modify_entry(label, None, None, None, Some(false));
        assert!(res.is_ok());
        assert_eq!(hosts.format_all(), expected);
    }

    #[test]
    fn remove_entry_test() {
        let expected = r####"192.168.1.10 duplicate
127.0.0.1 localhost

##
# IP V6 localhost
# #
::1 localhost

### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

"####;

        let mut hosts = HostsFile::from_hosts_file(String::from(get_sample_file()));
        let label = "example2".parse().unwrap();
        let res = hosts.remove_entry(&label);
        assert!(res.is_ok());
        assert_eq!(hosts.format_all(), expected);
    }

    #[test]
    fn remove_entry_not_found() {
        let mut hosts = HostsFile::from_hosts_file(String::from(get_sample_file()));
        let label = "not-found".parse().unwrap();
        let res = hosts.remove_entry(&label);
        let expected = String::from("No entry found with label 'not-found'");
        assert_eq!(res.unwrap_err(), expected);
    }

    #[test]
    fn add_host_test() {
        let original = r####"192.168.1.10 localsite


### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

"####;

        let expected = r####"192.168.1.10 localsite


### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

### <hostmanager label="example2" comment=hi there>
192.168.1.11 notused

"####;
        let mut hosts = HostsFile::from_hosts_file(String::from(original));
        let label = "example2".parse().unwrap();
        let ip = IpAddr::V4("192.168.1.11".parse().unwrap());
        let hostnames = "notused\n".parse().unwrap();
        let comment = "hi there".parse().unwrap();

        let res = &hosts.add_new_entry(label, ip, hostnames, comment);

        assert!(res.is_ok());
        assert_eq!(&hosts.format_all(), expected);
    }

    #[test]
    fn status_basic_test() {
        let original = r####"192.168.1.10 localsite


### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

### <hostmanager label="example2" comment=example2>
# 127.0.0.1 not.example.com

"####;

        let hosts = HostsFile::from_hosts_file(String::from(original));
        let res = &hosts.status(None).unwrap();

        assert!(res.contains("example"));
        assert!(res.contains("(active)\nIP Address: 127.0.0.1\nHosts: example.com foo bar\nComment: example"));
        assert!(res.contains("example2"));
        assert!(res.contains("(inactive)\nIP Address: 127.0.0.1\nHosts: not.example.com\nComment: example2\n\n"));
    }

    #[test]
    fn status_named_test() {
        let original = r####"192.168.1.10 localsite


### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

### <hostmanager label="example2" comment=example2>
# 127.0.0.1 not.example.com

"####;

        let hosts = HostsFile::from_hosts_file(String::from(original));
        let label = "example2".parse().unwrap();
        let res = &hosts.status(Some(&label)).unwrap();

        assert!(res.contains("example2"));
        assert!(res.contains("(inactive)"));
        assert!(res.contains("IP Address: 127.0.0.1\nHosts: not.example.com\nComment: example2\n\n"));
    }

    #[test]
    fn status_inactive_test() {
        let original = r####"192.168.1.10 localsite


### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

### <hostmanager label="example2" comment=example2>
# 127.0.0.1 not.example.com

"####;

        let mut hosts = HostsFile::from_hosts_file(String::from(original));
        hosts.set_filter(Some(false));
        let res = &hosts.status(None).unwrap();

        assert!(res.contains("example2"));
        assert!(res.contains("(inactive)"));
        assert!(res.contains("IP Address: 127.0.0.1\nHosts: not.example.com\nComment: example2\n\n"));
    }

    #[test]
    fn status_active_test() {
        let original = r####"192.168.1.10 localsite


### <hostmanager label="example" comment=example>
127.0.0.1 example.com foo bar

### <hostmanager label="example2" comment=example2>
# 127.0.0.1 not.example.com

"####;
        let mut hosts = HostsFile::from_hosts_file(String::from(original));
        hosts.set_filter(Some(true));
        let res = &hosts.status(None).unwrap();

        assert!(res.contains("example"));
        assert!(res.contains("(active)"));
        assert!(res.contains("IP Address: 127.0.0.1\nHosts: example.com foo bar\nComment: example\n\n"));
    }

}
