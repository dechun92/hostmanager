//! This module contains the `Label` type used to identify managed host entries.

use std::fmt;
use std::str::FromStr;
use std::error::Error;
use parser::valid_label;
use printer::Printable;

/// Struct used to give a meaningful label to a managed host entry.
#[derive(Clone, Debug, Eq, Hash, Ord, PartialEq, PartialOrd)]
pub struct Label {
    pub name: String,
}

impl Label {

    /// Constructs a new `Label` from the given `&str`. This is used by the
    /// parsers to create a new `Label` *after* parsing, and should not
    /// be used directly.
    pub fn new(s: &str) -> Label {
        Label { name: String::from(s.trim()) }
    }

    /// The `Label` format used in the hosts file. `parser.rs` will
    /// recognize the `### <label name=mylabel>` comment and parse it as a
    /// managed host entry.
    pub fn format(&self) -> String {
        format!("label=\"{}\"", &self.name)
    }

    /// Checks if this `Label` name is the same as `other`.
    pub fn same_as(&self, other: &str) -> bool {
        &self.name == other
    }
}

impl fmt::Display for Label {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.name)
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct LabelParseError;

impl Error for LabelParseError {
    fn description(&self) -> &str {
        "invalid label: valid characters are A-z, 0-9, _, -, and ."
    }
}


impl fmt::Display for LabelParseError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Unable to parse the requested label. Valid characters are: A-z, 0-9, '-', '_', and '.'")
    }
}

impl FromStr for Label {
    type Err = LabelParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let label = valid_label(s.as_bytes())
            .to_result()
            .map_err(|_e| LabelParseError)?;

        // TODO: the label parser grabs as many valid characters as it can,
        // so we have a quick check that the full label is the same as the
        // requested `str`. eventually need to rewrite the parser
        if label.same_as(s) {
            Ok(label)
        }
        else {
            Err(LabelParseError)
        }
    }
}

impl Printable for Label {

    /// This method is used to format a label for console output.
    fn format_console(&self) -> String {
        format!("{}", self.name)
    }
}


#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_label_format() {
        let label = Label::new("test");
        let expected = String::from("label=\"test\"");
        assert_eq!(label.format(), expected);
    }

    #[test]
    fn test_label_same_as() {
        let label = Label::new("test");
        assert!(label.same_as("test"));
    }

    #[test]
    fn parse_valid_label() {
        let res: Label = "foo".parse().unwrap();
        assert_eq!(res, Label::new("foo"));
    }

    #[test]
    fn parse_invalid_label() {
        let res: Result<Label, _> = "fo?o".parse();
        assert!(res.is_err());
    }
}
