//! A trait similar to `Display` but designed to format output for the console.

use console::style;

pub trait Printable {

    /// Create a `String` from the given type suitable for console output.
    fn format_console(&self) -> String;

    /// Helper to format in bold green or bold red depending on `status`.
    fn format_by_status(&self, status: bool) -> String {
        if status {
            format!("{}", style(&self.format_console()).bold().green())
        } else {
            format!("{}", style(&self.format_console()).bold().red())
        }
    }

}
