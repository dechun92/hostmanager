//! This module wraps up most of the `clap` crate's functionality to provide
//! our `main.rs` and `cli.rs` modules with a nicely structured command line
//! app.
use clap::{Arg, App, SubCommand};


/// The `get_app_matches` function is a helper that builds up all of our app's
/// subcommands and then gets the actual matches based on how the program
/// was invoked.
pub fn get_app<'a, 'b>() -> App<'a, 'b> {
    let add = SubCommand::with_name("add")
        .about("adds a host entry")
        .arg(label(true))
        .arg(ip(true))
        .arg(hosts(true))
        .arg(comment());

    let modify = SubCommand::with_name("modify")
        .about("modify an entry")
        .arg(label(true))
        .arg(ip(false))
        .arg(hosts(false))
        .arg(comment())
        .arg(active().help("enable this entry"))
        .arg(inactive().help("disable this entry"));

    let remove = SubCommand::with_name("remove")
        .about("removes a host entry")
        .arg(label(true));

    let rename = SubCommand::with_name("rename")
        .about("change the label for a managed entry")
        .arg(label(true))
        .arg(new_label());

    let status = SubCommand::with_name("status")
        .about("check the status of one or more entries")
        .arg(label(false))
        .arg(active().help("find active entries only"))
        .arg(inactive().help("find inactive entries only"));


    // the new `App` instance with all of our subcommands
    App::new("hosts")
        .version("0.1.0")
        .author("Eric Rasmussen")
        .about("friendly host switching and editing")
        .subcommand(add)
        .subcommand(modify)
        .subcommand(remove)
        .subcommand(rename)
        .subcommand(status)
        .after_help("NOTE: you can set the HOSTS_FILE env variable to use a file other than /etc/hosts\n")
}

/// Creates the label argument used by most commands.
fn label<'a, 'b>(required: bool) -> Arg<'a, 'b> {
    Arg::with_name("label")
        .required(required)
        .takes_value(true)
        .help("a unique identifier for an entry")
}

/// Constructs the new label argument used for renames.
fn new_label<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("new label")
        .takes_value(true)
        .required(true)
        .help("the new name for the given entry")
}

/// The IP address used in /etc/host entries.
fn ip<'a, 'b>(required: bool) -> Arg<'a, 'b> {
    Arg::with_name("ip")
        .short("i")
        .long("ip")
        .required(required)
        .takes_value(true)
        .help("the IP address for the entry")
}

/// A space-delimited list containing the hostname/aliases to override.
fn hosts<'a, 'b>(required: bool) -> Arg<'a, 'b> {
    Arg::with_name("hosts")
        .short("h")
        .long("hosts")
        .required(required)
        .takes_value(true)
        .help("hostname and aliases (separated by spaces)")
}

/// An optional comment for an entry, which will be printed out in status
/// commands.
fn comment<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("comment")
        .short("c")
        .long("comment")
        .required(false)
        .takes_value(true)
        .help("an optional annotation (displayed in status commands)")
}

/// Users can use --active when modifying an entry to turn the override on.
fn active<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("active")
        .long("active")
        .takes_value(false)
        .required(false)
        .conflicts_with("inactive")
}

/// Users can use --inactive when modifying an entry to turn the override off.
fn inactive<'a, 'b>() -> Arg<'a, 'b> {
    Arg::with_name("inactive")
        .long("inactive")
        .takes_value(false)
        .required(false)
        .conflicts_with("active")
}

// although we trust the `clap` crate to handle required and conflicting values,
// we don't want to accidentally break one of those guarantees when modifying
// the above program definition. these tests are here to verify the most
// important guarantees only, and aren't meant to be complete
#[cfg(test)]
mod test {
    use super::*;
    use clap::ErrorKind;

    #[test]
    fn test_add_missing_label() {
        let args = vec!["hosts", "add", "--ip", "127.0.0.1", "--hosts", "host"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("<label>"));
    }

    #[test]
    fn test_add_missing_ip() {
        let args = vec!["hosts", "add", "foo", "--hosts", "host"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("--ip <ip>"));
    }

    #[test]
    fn test_add_missing_hosts() {
        let args = vec!["hosts", "add", "foo", "--ip", "127.0.0.1"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        println!("{:?}", res.message);
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("--hosts <hosts>"));
    }

    #[test]
    fn test_modify_missing_label() {
        let args = vec!["hosts", "modify", "--ip", "127.0.0.1"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("<label>"));
    }

    #[test]
    fn test_modify_status_conflicts() {
        let args = vec!["hosts", "modify", "foo", "--active", "--inactive"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert_eq!(res.kind, ErrorKind::ArgumentConflict);
    }

    #[test]
    fn test_remove_missing_label() {
        let args = vec!["hosts", "remove"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("<label>"));
    }

    #[test]
    fn test_rename_missing_label() {
        let args = vec!["hosts", "rename"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("<label>"));
    }

    #[test]
    fn test_rename_missing_new_label() {
        let args = vec!["hosts", "rename", "foo"];
        let res = get_app().get_matches_from_safe(args).unwrap_err();
        assert!(res.message.contains("The following required arguments were not provided"));
        assert!(res.message.contains("<new label>"));
    }
}
