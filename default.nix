with import <nixpkgs> {};

with rustPlatform;

buildRustPackage rec {

  name = "hostmanager-${version}";
  version = "0.1.0";

  src = ./.;

  cargoSha256 = "0fkma6bpz530ry0hqkyxswv5yh0khcicwf4v5b1an6dgayrd3f6h";

  meta = {
    description = "Command-line interface for managing /etc/hosts entries";
    inherit version;
  };
}
