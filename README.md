# Hostmanager

Hostmanager is a command line utility for working with the `/etc/hosts` file.

Although the `/etc/hosts` file is easy to edit by hand, when you make many
edits a day it can be easy to lose track. `hostmanager` provides a color-coded,
interactive command-line interface that makes it easy to quickly check on the
status of an entry or make changes to it.


# Installation

The program comes with one binary, named `hostmanager`. It can be installed
via [nix](https://nixos.org/nix/) or [cargo](http://doc.crates.io/).

## With nix

```
git clone https://gitlab.com/theerasmas/hostmanager.git
nix-env -f hostmanager/ -i hostmanager
```

## With cargo

```
git clone https://gitlab.com/theerasmas/hostmanager.git
cd hostmanager
cargo install
```

# Example usage

## Help

```
$ hostmanager --help
hosts 0.1.0
Eric Rasmussen
friendly host switching and editing

USAGE:
    hostmanager [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

SUBCOMMANDS:
    add       adds a host entry
    help      Prints this message or the help of the given subcommand(s)
    modify    modify an entry
    remove    removes a host entry
    rename    change the label for a managed entry
    status    check the status of one or more entries

NOTE: you can set the HOSTS_FILE env variable to use a file other than /etc/hosts
```

## Add a new entry

```
$ sudo hostmanager add somewebsite --ip 127.0.0.1 --hosts somewebsite --comment "stop going to some_site"
Entry added!

somewebsite (active)
IP Address: 127.0.0.1
Hosts: somewebsite
Comment: stop going to some_site
```

## Modify an entry: using --inactive to turn it off

```
$ sudo hostmanager modify somewebsite --inactive
Entry modified!

somewebsite (inactive)
IP Address: 127.0.0.1
Hosts: somewebsite
Comment: stop going to some_site
```

## Modify an entry: activate it and also change the hosts

```
$ sudo hostmanager modify somewebsite --hosts "somewebsite alias1 alias2" --active
Entry modified!

somewebsite (active)
IP Address: 127.0.0.1
Hosts: somewebsite alias1 alias2
Comment: stop going to some_site
```

Note: you can optionally change any of `--hosts`, `--ip`, `--active` or
`--inactive`, and `--comment`.

## Rename a host

```
$ sudo hostmanager rename somewebsite thatwebsite
Entry modified!

thatwebsite (active)
IP Address: 127.0.0.1
Hosts: somewebsite alias1 alias2
Comment: stop going to some_site
```

## Checking the current status of managed entries

You can use the form ``hostmanager status some_entry`` for a single status,
or ``hostmanager status`` to see all managed entries.

```
$ sudo hostmanager status
thatwebsite (active)
IP Address: 127.0.0.1
Hosts: somewebsite alias1 alias2
Comment: stop going to some_site
```

## Remove an entry

```
$ sudo hostmanager remove thatwebsite
Entry thatwebsite removed!
```

# Building the docs

The private modules are documented, and you can view the API docs
using the following command from the project root directory:

```
cargo rustdoc -- --no-defaults --passes strip-hidden --passes collapse-docs \
  --passes unindent-comments --passes strip-priv-imports
```

The docs will be available in ``<project root>``/target/doc/hostmanager/index.html


# Program goals

`hostmanager` is designed to make your life easier if you frequently change
hosts files. This can be helpful if you work on many different deployments
with sites that match on hostname (most often apache using the ServerName
directive or nginx using server_name).

If you do not frequently change the hosts file, it's probably a whole lot easier
to edit it as needed by hand.

This project was also written to utilize and show off the many powerful
features of the rust language and some of the more popular crates like
`nom`, `clap`, and `console`.



